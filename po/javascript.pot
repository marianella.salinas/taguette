# Translations template for Taguette.
# Copyright (C) 2018 Remi Rampin and Taguette contributors
# This file is distributed under the same license as the Taguette project.
# Remi Rampin <remi@rampin.org>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Taguette\n"
"Report-Msgid-Bugs-To: hi@taguette.org\n"
"POT-Creation-Date: 2022-10-07 12:08-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: taguette/static/js/taguette.js:506
msgid "Couldn't update project metadata!"
msgstr ""

#: taguette/static/js/taguette.js:562 taguette/static/js/taguette.js:839
msgid "Edit"
msgstr ""

#: taguette/static/js/taguette.js:571
msgid "There are no documents in this project yet."
msgstr ""

#: taguette/static/js/taguette.js:665 taguette/static/js/taguette.js:671
msgid "Error uploading file!"
msgstr ""

#: taguette/static/js/taguette.js:703
msgid "Document name cannot be empty"
msgstr ""

#: taguette/static/js/taguette.js:720
msgid "Couldn't update document!"
msgstr ""

#: taguette/static/js/taguette.js:729
#, python-format
msgid "Are you sure you want to delete the document '%(doc)s'?"
msgstr ""

#: taguette/static/js/taguette.js:741
msgid "Couldn't delete document!"
msgstr ""

#: taguette/static/js/taguette.js:848
msgid "There are no tags in this project yet."
msgstr ""

#: taguette/static/js/taguette.js:888
msgid "no tags"
msgstr ""

#: taguette/static/js/taguette.js:959
msgid "Invalid tag name"
msgstr ""

#: taguette/static/js/taguette.js:986
msgid "Couldn't create tag!"
msgstr ""

#: taguette/static/js/taguette.js:995
#, python-format
msgid "Are you sure you want to delete the tag '%(tag)s'?"
msgstr ""

#: taguette/static/js/taguette.js:1010
msgid "Couldn't delete tag!"
msgstr ""

#: taguette/static/js/taguette.js:1079
msgid "Couldn't merge tags!"
msgstr ""

#. Key used to create a new highlight
#: taguette/static/js/taguette.js:1186
msgid "n"
msgstr ""

#: taguette/static/js/taguette.js:1249
msgid "Couldn't create highlight!"
msgstr ""

#: taguette/static/js/taguette.js:1269
msgid "Couldn't delete highlight!"
msgstr ""

#: taguette/static/js/taguette.js:1300
msgid "Full permissions"
msgstr ""

#: taguette/static/js/taguette.js:1301
msgid "Can't change collaborators / delete project"
msgstr ""

#: taguette/static/js/taguette.js:1302
msgid "View & make changes"
msgstr ""

#: taguette/static/js/taguette.js:1303
msgid "View only"
msgstr ""

#: taguette/static/js/taguette.js:1374
msgid "Already a member!"
msgstr ""

#: taguette/static/js/taguette.js:1392
msgid "This user doesn't exist!"
msgstr ""

#: taguette/static/js/taguette.js:1435
msgid "Couldn't update collaborators!"
msgstr ""

#: taguette/static/js/taguette.js:1463
msgid "Load a document on the left"
msgstr ""

#: taguette/static/js/taguette.js:1531
msgid "Error loading document!"
msgstr ""

#: taguette/static/js/taguette.js:1594
msgid "No highlights with this tag yet."
msgstr ""

#: taguette/static/js/taguette.js:1682
msgid "Error loading tag highlights!"
msgstr ""

#: taguette/static/js/taguette.js:1835
msgid "It appears that you have been logged out."
msgstr ""

#: taguette/static/js/taguette.js:1838
msgid "You can no longer access this project."
msgstr ""

